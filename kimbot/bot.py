#!/usr/bin/env python
# -*- coding: utf-8 -*-
# kimbot
# Copyright © 2018 Ruben Di Battista
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import logging
import os

from dotenv import load_dotenv

from telegram.ext import Updater, CommandHandler

load_dotenv()

CONFIG_FILE = "users.json"
TELEGRAM_BOT_TOKEN = os.environ["TELEGRAM_BOT_TOKEN"]

logger = logging.getLogger(__name__)


def start(bot, update):
    """Welcoming entry point"""

    update.message.reply_text(
        "Hello, welcome here. Use the /subscribe command to subscribe to "
        "the broadcasting list of Kimsufi servers so that you can get "
        "notified when the Kimsufi server that you chose is available"
    )


def subscribe(bot, update):
    """This command registers a users in the broadcast queue"""
    # Load previous configuration file
    try:
        with open(CONFIG_FILE, "r") as fr:
            users = json.load(fr)
    except IOError:
        users = []

    # Append chat_id to configuration_file
    with open(CONFIG_FILE, "w") as config_file:
        logger.info(f"{users}")
        if update.message.chat_id not in users:
            users.append(update.message.chat_id)
        json.dump(users, config_file)

    update.message.reply_text("I just saved your chat_id")


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    updater = Updater(TELEGRAM_BOT_TOKEN)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("subscribe", subscribe))

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
