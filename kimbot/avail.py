#!/usr/bin/env python
# -*- coding: utf-8 -*-
# kimbot
# Copyright © 2018 Ruben Di Battista
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
This scripts interrogates the OVH api for availabilities of servers and post
the results into a Telegram Bot

Usage:
    kim-avail [-v <verbosity>] -C <country> <hardware> ...

Options:
    -C, --country=<country>         The datacenter country to search into
    -v, --verbosity=<verbosity>     The level of verbosity for logging
                                    [default: INFO]

kimbot
Copyright © 2018 Ruben Di Battista

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""


import json
import logging
import logging.handlers
import sys

import requests
import telegram
from bot import TELEGRAM_BOT_TOKEN
from docopt import docopt

BOT_MESSAGE = """\
Hey! Good news, the Kimsufi server you asked for is available.
Hardware: **{hardware}**
Available in zones: **{zones}**
Order: https://www.kimsufi.com/it/ordine/kimsufi.xml?reference={hardware}
"""


API_ENDPOINT = "https://www.ovh.com/engine/api/dedicated/server/availabilities"


if __name__ == "__main__":
    args = docopt(__doc__)

    hardwares = args["<hardware>"]
    country = args["--country"]
    verbosity = args["--verbosity"]
    verbosity = getattr(logging, verbosity.upper())

    bot = telegram.Bot(token=TELEGRAM_BOT_TOKEN)

    # Setup Logging
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # Debug Logging also for requests
    requests_log = logging.getLogger("urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

    formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(name)s - %(module)s - %(message)s"
    )

    # Log on file with rotation
    fh = logging.handlers.RotatingFileHandler(
        "kimbot.log", backupCount=2, maxBytes=100 * 1024 * 1024
    )
    fh.setFormatter(formatter)
    fh.setLevel(verbosity)
    logger.addHandler(fh)

    # Connection
    try:
        r = requests.get(API_ENDPOINT, params={"country": country.lower()})
    except requests.exceptions.RequestException as e:
        logger.error(f"HTTP request failed with: {e}")
        sys.exit(1)
    logger.debug("Response:")
    logger.debug(json.dumps(r.json(), indent=4, sort_keys=True))
    availability = r.json()

    found = False
    for item in availability:
        # Looping over the given hardwares
        for h in hardwares:
            # Check if user asked for it
            if not (h == item["hardware"]):
                continue

            # For each item let's check which zones are available
            found = True
            available_zones = set(
                [
                    datacenter["datacenter"]
                    for datacenter in item["datacenters"]
                    if datacenter["availability"] not in ["unavailable", "unknown"]
                ]
            )

            logger.debug(
                "Available zones for {item}".format(
                    item=json.dumps(item, indent=4, sort_keys=True)
                )
            )
            logger.debug(available_zones)

            if available_zones:
                # Notify availability
                bot.send_message(
                    chat_id=26325297,
                    text=BOT_MESSAGE.format(hardware=h, zones=available_zones),
                    parse_mode=telegram.ParseMode.MARKDOWN,
                )
            else:
                logging.info(f"No availability found for the hardware {h}")

    if not (found):
        logging.info(f"{hardwares} were not listed in the API response")
